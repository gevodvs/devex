package com.hevod.devexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevExamApplication {

    public static void main(String[] args) {
        SpringApplication.run(DevExamApplication.class, args);
    }

}
