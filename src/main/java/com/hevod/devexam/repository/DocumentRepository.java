package com.hevod.devexam.repository;

import com.hevod.devexam.entity.Document;
import com.hevod.devexam.entity.DocumentType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface DocumentRepository extends CrudRepository<Document, Long> {

    List<Document> findAllByClientIdOrderByCreationDate(Long clientId);

    @Query(value = "SELECT * FROM docs where client_id = :clientId " +
            "AND data like (%:data%) ORDER BY creation_date", nativeQuery = true)
    List<Document> findAllByClientIdAndData(Long clientId, String data);

    List<Document> findAllByClientIdAndDataIsContainingIgnoreCaseAndTypeOrderByCreationDate(Long clientId, String data, DocumentType type);

    List<Document> findAllByClientIdAndApprovalDateIsNotNullOrderByCreationDate(Long clientId);

    List<Document> findAllByClientIdAndCreationDateIsAfterAndCreationDateIsBeforeOrderByCreationDate(Long clientId,
                                                                                                     LocalDateTime before,
                                                                                                     LocalDateTime after);

}
