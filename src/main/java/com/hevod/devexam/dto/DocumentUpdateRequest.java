package com.hevod.devexam.dto;

import io.swagger.v3.oas.annotations.Parameter;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DocumentUpdateRequest {

    @NotBlank
    @Parameter(description = "new doc payload", required = true)
    private String data;
}
