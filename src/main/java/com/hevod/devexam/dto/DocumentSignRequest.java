package com.hevod.devexam.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class DocumentSignRequest {
    @NotBlank
    private Long signerId;
}
