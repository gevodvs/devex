package com.hevod.devexam.dto;

import java.time.LocalDateTime;

public record DocumentSaveResponse(Long id, LocalDateTime createTime) {
}
