package com.hevod.devexam.dto;

import com.hevod.devexam.entity.DocumentType;
import lombok.Data;

@Data
public class DocumentFindRequest {

    private String name;
    private DocumentType type;
    private String data;
    private String owner;
}
