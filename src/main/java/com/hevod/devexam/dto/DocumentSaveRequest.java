package com.hevod.devexam.dto;

import com.hevod.devexam.entity.DocumentType;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class DocumentSaveRequest {

    @NotBlank
    @Parameter(description = "name of the document", required = true)
    private String name;
    @Parameter(description = "type of the doc", required = true)
    @NotBlank
    private DocumentType type;
    @Parameter(description = "document payload", required = true)
    @NotBlank
    private String data;
    @Parameter(description = "document creator", required = true)
    @NotNull
    private Long ownerId;
}
