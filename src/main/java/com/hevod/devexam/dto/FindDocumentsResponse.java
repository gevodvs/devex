package com.hevod.devexam.dto;

import com.hevod.devexam.entity.Document;

import java.util.List;

public record FindDocumentsResponse(List<Document> docs) {
}
