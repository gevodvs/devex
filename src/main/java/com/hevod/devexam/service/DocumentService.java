package com.hevod.devexam.service;

import com.hevod.devexam.dto.DocumentSaveRequest;
import com.hevod.devexam.dto.DocumentSaveResponse;
import com.hevod.devexam.dto.DocumentSignRequest;
import com.hevod.devexam.dto.DocumentUpdateRequest;
import com.hevod.devexam.dto.FindDocumentsResponse;
import com.hevod.devexam.entity.Document;
import com.hevod.devexam.entity.DocumentType;
import com.hevod.devexam.exception.BasicLogicException;
import com.hevod.devexam.exception.ErrorCode;
import com.hevod.devexam.repository.DocumentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;

import static org.apache.commons.lang3.StringUtils.isBlank;

@Service
@RequiredArgsConstructor
public class DocumentService {

    private final DocumentRepository documentRepository;

    public DocumentSaveResponse save(DocumentSaveRequest request) {
        Document document = documentRepository.save(toDocument(request));
        return new DocumentSaveResponse(document.getId(), document.getCreationDate());
    }

    public void update(Long id, Long clientId, DocumentUpdateRequest request) {
        documentRepository.findById(id)
                .ifPresentOrElse(document -> {
                            if (!Objects.equals(document.getClientId(), clientId))
                                throw new BasicLogicException(ErrorCode.ACCESS_ERROR, "document doesnt belong to client");
                            documentRepository.save(map(document, request));
                        },
                        () -> {
                            throw new BasicLogicException(ErrorCode.DOCUMENT_NOT_FOUND, "Cant find document by id" + id);
                        });
    }

    public void delete(Long id) {
        documentRepository.deleteById(id);
    }

    public void sign(Long documentId, DocumentSignRequest documentSignRequest) {
        documentRepository.findById(documentId)
                .ifPresentOrElse(document ->
                                documentRepository.save(map(document, documentSignRequest)),
                        () -> {
                            throw new BasicLogicException(ErrorCode.DOCUMENT_NOT_FOUND, "Cant find document by id" + documentId);
                        });
    }

    public FindDocumentsResponse findAllByOwner(Long owner) {
        List<Document> allByOwner = documentRepository.findAllByClientIdOrderByCreationDate(owner);
        if (allByOwner.isEmpty())
            throw new BasicLogicException(ErrorCode.DOCUMENT_NOT_FOUND, "Cant find document by owner" + owner);
        return new FindDocumentsResponse(allByOwner);
    }

    public FindDocumentsResponse findAllByParams(Long clientId,
                                                 String data,
                                                 String type) {

        if (isBlank(data) && isBlank(type)) {
            List<Document> allByOwner = documentRepository.findAllByClientIdOrderByCreationDate(clientId);
            if (allByOwner.isEmpty())
                throw new BasicLogicException(ErrorCode.DOCUMENT_NOT_FOUND, "Cant find document by owner");
            return new FindDocumentsResponse(allByOwner);
        } else if (isBlank(type)) {
            List<Document> allByOwner = documentRepository.findAllByClientIdAndData(clientId, data);
            if (allByOwner.isEmpty())
                throw new BasicLogicException(ErrorCode.DOCUMENT_NOT_FOUND, "Cant find document by owner and data");
            return new FindDocumentsResponse(allByOwner);
        } else {
            List<Document> allByOwner = documentRepository.findAllByClientIdAndDataIsContainingIgnoreCaseAndTypeOrderByCreationDate(clientId, data, DocumentType.valueOf(type));
            if (allByOwner.isEmpty())
                throw new BasicLogicException(ErrorCode.DOCUMENT_NOT_FOUND, "Cant find document by owner and data and type");
            return new FindDocumentsResponse(allByOwner);
        }


    }

    public FindDocumentsResponse findAllByOwnerAndApprovalDateIsNotNull(Long clientId) {
        return new FindDocumentsResponse(documentRepository.findAllByClientIdAndApprovalDateIsNotNullOrderByCreationDate(clientId));
    }

    public FindDocumentsResponse findAllByOwnerAndApprovalDateIsNotNull(Long clientId, LocalDateTime from, LocalDateTime to) {
        return new FindDocumentsResponse(documentRepository.findAllByClientIdAndCreationDateIsAfterAndCreationDateIsBeforeOrderByCreationDate(clientId, from, to));
    }

    private Document toDocument(DocumentSaveRequest documentSaveRequest) {
        Document document = new Document();
        document.setData(documentSaveRequest.getData());
        document.setName(documentSaveRequest.getName());
        document.setClientId(documentSaveRequest.getOwnerId());
        document.setType(documentSaveRequest.getType());
        document.setCreationDate(LocalDateTime.now());
        return document;
    }

    private Document map(Document document, DocumentUpdateRequest documentUpdateRequest) {
        document.setData(documentUpdateRequest.getData());
        return document;
    }

    private Document map(Document document, DocumentSignRequest documentSignRequest) {
        document.setApprovalDate(LocalDateTime.now());
        document.setSigner(documentSignRequest.getSignerId());
        return document;
    }
}
