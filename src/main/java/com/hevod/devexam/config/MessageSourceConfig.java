package com.hevod.devexam.config;

import com.hevod.devexam.exception.ErrorMessageManager;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ResourceBundleMessageSource;

@Configuration
public class MessageSourceConfig {

    private static final String ERROR_MESSAGE_BUNDLE_NAME = "ErrorMessages";


    @Bean(ERROR_MESSAGE_BUNDLE_NAME)
    public MessageSource getErrorMessageSource() {
        ResourceBundleMessageSource resourceBundleMessageSource = new ResourceBundleMessageSource();
        resourceBundleMessageSource.setFallbackToSystemLocale(false);
        resourceBundleMessageSource.setBasename(ERROR_MESSAGE_BUNDLE_NAME);
        ErrorMessageManager.setERROR_MESSAGE_SOURCE(resourceBundleMessageSource);
        return resourceBundleMessageSource;
    }
}
