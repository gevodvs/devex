package com.hevod.devexam.config;

import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SwaggerConfig {

    private static final String DOCKET_API_BASE_PACKAGE = "com.hevod.devexam";

    @Bean
    public GroupedOpenApi paymentsApi() {
        return GroupedOpenApi.builder()
                .packagesToScan(DOCKET_API_BASE_PACKAGE)
                .group("devexam")
                .pathsToMatch("/api/**")
                .build();
    }
}
