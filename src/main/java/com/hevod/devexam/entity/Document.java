package com.hevod.devexam.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;


@Entity
@Data
@Table(name = "docs")
public class Document {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false)
    private String name;

    @Enumerated(value = EnumType.STRING)
    private DocumentType type;
    @Column(nullable = false)
    private String data;
    @Column(nullable = false)
    private LocalDateTime creationDate;
    private LocalDateTime approvalDate;
    @Column(nullable = false)
    private Long clientId;
    private Long signer;
}
