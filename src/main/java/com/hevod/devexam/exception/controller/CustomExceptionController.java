package com.hevod.devexam.exception.controller;

import com.hevod.devexam.exception.BasicLogicException;
import com.hevod.devexam.exception.ErrorCode;
import com.hevod.devexam.exception.ErrorMessageManager;
import com.hevod.devexam.exception.ErrorResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
@Order(1)
public class CustomExceptionController {

    @ExceptionHandler(BasicLogicException.class)
    public ResponseEntity<ErrorResponse> handleBasicLogicException(BasicLogicException bex) {
        log.error(bex.getMessage(), bex);


        final ErrorCode errorCode = bex.getErrorCode();

        ErrorResponse errorResponse = ErrorResponse.builder()
                .errCode(errorCode.name())
                .errText(ErrorMessageManager.getMessage(errorCode))
                .errTitle(ErrorMessageManager.getTitle(errorCode))
                .build();

        return new ResponseEntity<>(
                errorResponse,
                errorCode.getHttpStatus());
    }
}
