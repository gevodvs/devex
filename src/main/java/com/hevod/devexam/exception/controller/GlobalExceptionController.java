package com.hevod.devexam.exception.controller;

import com.hevod.devexam.exception.ErrorCode;
import com.hevod.devexam.exception.ErrorMessageManager;
import com.hevod.devexam.exception.ErrorResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
@Order(2)
public class GlobalExceptionController {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ErrorResponse> handleException(Exception ex) {
        log.error(ex.getMessage(), ex);

        return new ResponseEntity<>(
                new ErrorResponse(
                        ErrorCode.DEFAULT_ERROR,
                        ErrorMessageManager.getMessage(ErrorCode.DEFAULT_ERROR)),
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
