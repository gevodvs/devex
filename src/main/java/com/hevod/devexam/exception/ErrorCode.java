package com.hevod.devexam.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;

@Getter
@RequiredArgsConstructor
public enum ErrorCode {

    DEFAULT_ERROR(HttpStatus.BAD_REQUEST),
    DOCUMENT_NOT_FOUND(HttpStatus.BAD_REQUEST),
    ACCESS_ERROR(HttpStatus.BAD_REQUEST);

    private final HttpStatus httpStatus;

}
