package com.hevod.devexam.exception;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serial;
import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorResponse implements Serializable {

    @Serial
    private static final long serialVersionUID = 0L;

    private String errCode;
    private String errText;
    private String errTitle;

    public ErrorResponse(ErrorCode errCode, String errTitle, String errText) {
        this.errCode = errCode == null ? null : errCode.name();
        this.errTitle = errTitle;
        this.errText = errText;
    }

    public ErrorResponse(ErrorCode errCode, String errText) {
        this.errCode = errCode == null ? null : errCode.name();
        this.errText = errText;
    }
}