package com.hevod.devexam.exception;

import lombok.Setter;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;

import java.util.Locale;

@UtilityClass
@Slf4j
public class ErrorMessageManager {
    private static final String ERROR_TITLE_POSTFIX = ".errTitle";
    private static final String ERROR_TEXT_POSTFIX = ".errText";
    private static final Locale DEFAULT_LOCALE = Locale.ENGLISH;
    @Setter
    private static MessageSource ERROR_MESSAGE_SOURCE;


    public String getMessage(ErrorCode code) {
        if (ERROR_MESSAGE_SOURCE == null) {
            log.error("MessageBundle for Exceptions not specified");
            return null;
        }
        return getMessageText(code, LocaleContextHolder.getLocale());
    }

    public String getTitle(ErrorCode code) {
        if (ERROR_MESSAGE_SOURCE == null) {
            log.error("MessageBundle for Exceptions not specified");
            return null;
        }
        return getMessageTitle(code, LocaleContextHolder.getLocale());


    }


    private String getMessageTitle(ErrorCode errorCode, Locale locale) {
        return ERROR_MESSAGE_SOURCE.getMessage(
                errorCode.name().concat(ERROR_TITLE_POSTFIX),
                null,
                getDefaultMessageTitle(),
                locale);
    }


    private String getMessageText(ErrorCode errorCode, Locale locale) {
        return ERROR_MESSAGE_SOURCE.getMessage(
                errorCode.name().concat(ERROR_TEXT_POSTFIX),
                null,
                getDefaultMessageText(),
                locale);
    }

    public boolean isLocalizationErrorCode(ErrorCode errorCode) {
        return ERROR_MESSAGE_SOURCE.getMessage(errorCode.name().concat(ERROR_TEXT_POSTFIX), null, null, DEFAULT_LOCALE)
                != null;
    }


    public String getDefaultMessageTitle() {
        return ERROR_MESSAGE_SOURCE.getMessage(
                ErrorCode.DEFAULT_ERROR.name().concat(ERROR_TITLE_POSTFIX),
                null,
                null,
                LocaleContextHolder.getLocale());
    }

    public String getDefaultMessageText() {
        return ERROR_MESSAGE_SOURCE.getMessage(
                ErrorCode.DEFAULT_ERROR.name().concat(ERROR_TEXT_POSTFIX),
                null,
                null,
                LocaleContextHolder.getLocale());
    }
}