package com.hevod.devexam.exception;

import lombok.Getter;

@Getter
public class BasicLogicException extends RuntimeException {

    private final ErrorCode errorCode;


    public BasicLogicException(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public BasicLogicException(ErrorCode errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }
}
