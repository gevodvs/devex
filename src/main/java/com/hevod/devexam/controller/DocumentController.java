package com.hevod.devexam.controller;

import com.hevod.devexam.dto.DocumentSaveRequest;
import com.hevod.devexam.dto.DocumentSaveResponse;
import com.hevod.devexam.dto.DocumentSignRequest;
import com.hevod.devexam.dto.DocumentUpdateRequest;
import com.hevod.devexam.dto.FindDocumentsResponse;
import com.hevod.devexam.service.DocumentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("api/v1/documents")
@RequiredArgsConstructor
public class DocumentController {

    private final DocumentService documentService;

    @PostMapping
    public DocumentSaveResponse save(@RequestBody DocumentSaveRequest documentSaveRequest) {
        return documentService.save(documentSaveRequest);
    }


    @PutMapping("/{id}")
    public void update(@PathVariable Long id,
                       @RequestHeader("client-id") Long clientId,
                       @RequestBody DocumentUpdateRequest documentUpdateRequest) {
        documentService.update(id, clientId, documentUpdateRequest);
    }

    @PostMapping("/sign/{id}")
    public void update(@PathVariable Long id,
                       @RequestBody DocumentSignRequest documentSignRequest) {
        documentService.sign(id, documentSignRequest);
    }

    @DeleteMapping("/{id}")
    public void update(@PathVariable Long id) {
        documentService.delete(id);
    }

    @GetMapping("/{clientId}")
    public FindDocumentsResponse findByOwner(@PathVariable Long clientId) {
        return documentService.findAllByOwner(clientId);
    }


    @GetMapping("/search/{clientId}")
    public FindDocumentsResponse find(@PathVariable Long clientId,
                                      @RequestParam(required = false) String data,
                                      @RequestParam(required = false) String type) {
        return documentService.findAllByParams(clientId, data, type);
    }


    @GetMapping("/approved/{clientId}")
    public FindDocumentsResponse findAllByOwnerAndApprovalDateIsNotNull(@PathVariable Long clientId) {
        return documentService.findAllByOwnerAndApprovalDateIsNotNull(clientId);
    }

    @GetMapping("/by-date/{clientId}")
    public FindDocumentsResponse findAllByOwnerAndDateBetween(@PathVariable Long clientId,
                                                              @RequestParam(required = false) LocalDateTime from,
                                                              @RequestParam LocalDateTime to) {
        return documentService.findAllByOwnerAndApprovalDateIsNotNull(clientId, from, to);
    }


}
